import './style.css'
import javascriptLogo from './javascript.svg'
import viteLogo from '/vite.svg'

import { rgb_to_hex, hex_to_rgb } from "./converter.js"

document.querySelector('#app').innerHTML = `
  <div>
    <a href="https://vitejs.dev" target="_blank">
      <img src="${viteLogo}" class="logo" alt="Vite logo" />
    </a>
    <a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript" target="_blank">
      <img src="${javascriptLogo}" class="logo vanilla" alt="JavaScript logo" />
    </a>
    <h1>RGB and HEX</h1>
    <label for="color-red">Red: </label>
    <input type="range" id="color-red" value="255" step="1" min="0" max="255"/>
    <output class="color-red-output" for="color-red">0</output>
    <div>

    <label for="color-red">Green:</label>
    <input type="range" id="color-green" value="255" step="1" min="0" max="255"/>
    <output class="color-green-output" for="color-green">0</output>
    </div>
    <div>
    <label for="color-red">Blue:</label>
    <input type="range" id="color-blue" value="255" step="1" min="0" max="255"/>
    <output class="color-blue-output" for="color-blue">0</output>
    </div>
    <div>
    <input type="text" id="color-hex" minlength="7" maxlength="7" value="#333333"/>
    </div>

    </div>
    `
    // <input type="number" value="255" min="0" max="255"/>
    // <input type="number" value="255" min="0" max="255"/>
    // <input type="number" value="255" min="0" max="255"/>
    // <input type="number" value="255" min="0" max="255"/>

// setupCounter(document.querySelector('#counter'));

/** @type {HTMLInputElement} */
const colors = ["red","green","blue"];
/** @type {HTMLInputElement[]} */
const colorInputs = [];
/** @type {HTMLInputElement[]} */
const colorOutputs = [];
/** @type {HTMLInputElement} */
const colorHex = document.querySelector("#color-hex");
colorHex.addEventListener("input", () => {
  const hex = colorHex.value;
  const rgb = hex_to_rgb(hex);
  // Conversion succeed
  if (rgb != null) {
    for (const [i, colorInput] of colorInputs.entries()) {
      colorInput.value = ""+rgb[i];
      colorOutputs[i].textContent = colorInput.value;
    }
  }
});
function updateHex() {
  const colorValues = [];
  for (const color of colorInputs) {
    colorValues.push(parseInt(color.value));
  }
  const hex = rgb_to_hex(colorValues[0], colorValues[1], colorValues[2]);
  console.log(hex);
  colorHex.value = hex.toUpperCase();
}
for (const colorName of colors) {
  const color = document.querySelector(`#color-${colorName}`);
  colorInputs.push(color);
  const output = document.querySelector(`.color-${colorName}-output`);
  colorOutputs.push(output);
  output.textContent = color.value;
  
  color.addEventListener("input", () => {
    output.textContent = color.value;
    updateHex();
  })
}
