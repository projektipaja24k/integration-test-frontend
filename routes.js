import {Router} from "express"
import { rgb_to_hex, hex_to_rgb } from "./converter.js";

const routes = Router();

routes.get("/", (req,res) => {
    res.status(200).send("Hello!");
});
routes.get("/test", (req,res) => {
    res.status(200).send("Test!");
});
routes.get("/rgb-to-hex", (req,res) => {
    // res.status(200).send("Not yet implemented!");
    const r = parseFloat(req.query.red);
    const g = parseFloat(req.query.green);
    const b = parseFloat(req.query.blue);

    const HEX = rgb_to_hex(r,g, b);

    res.status(200).send(`${HEX}`);
})
routes.get("/hex-to-rgb", (req,res) => {
    const HEX = req.query.hex;
    const RGB_ARRAY = hex_to_rgb(HEX);
    const ret = `rgb(${RGB_ARRAY[0]},${RGB_ARRAY[1]},${RGB_ARRAY[2]})`;
    res.status(200).send(ret);
})

export default routes;

