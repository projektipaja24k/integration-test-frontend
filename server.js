//e.g server.js
import express from "express";
import ViteExpress from "vite-express";

import routes from "./routes.js";

const app = express();

app.get("/message", (_, res) => res.send("Hello from express!"));
app.get("/api/v1", routes);

ViteExpress.listen(app, 8000, () => console.log("Server is listening..."));